# cryptojs2

JavaScript library of crypto standards.

## Node.js (Install)

Requirements:

- Node.js

```bash
npm install cryptojs2
```

### Usage

Node.js

```javascript
import sha256 from 'cryptojs2/sha256';
import hmacSHA256 from 'cryptojs2/hmac-sha256'; 
//or
import {SHA256 as sha256,HmacSHA256 as hmacSha256} from "cryptojs2";   

const message, nonce, path, privateKey; // ...
const hashDigest = sha256(nonce + message);
const hmacDigest = Base64.stringify(hmacSHA512(path + hashDigest, privateKey));
```
 
## Client (browser)

Requirements: 

### Usage

Modular include:
   

### Usage without RequireJS

```html 
<script type="module">
import sha256 from 'cryptojs2/sha256';
import hmacSHA256 from 'cryptojs2/hmac-sha256'; 
//or
import {SHA256 as sha256,HmacSHA256 as hmacSha256} from "cryptojs2";   

window.sha256 = sha256; //FOR USE ALL DOCUMENT
window.hmacSHA256 = hmacSHA256; //FOR USE ALL DOCUMENT
    var hashSHA256 = sha256("Message");
    var hashhmacSha256 =hmacSha256("stringToSign", "signingKey");
</script>
```
 

### AES Encryption

#### Plain text encryption

```javascript
import aes from 'cryptojs2/AES';
import hmacSHA256 from 'cryptojs2/hmac-sha256'; 

// Encrypt
var ciphertext = aes.encrypt('message', 'secret key').toString();

// Decrypt
var bytes  = aes.decrypt(ciphertext, 'secret key');
var originalText = bytes.toString();

console.log(originalText); // 'message'
```

#### Object encryption

```javascript
import aes from 'cryptojs2/AES';

var data = [{id: 1}, {id: 2}]

// Encrypt
var ciphertext = aes.encrypt(JSON.stringify(data), 'secret key 123').toString();

// Decrypt
var bytes  = aes.decrypt(ciphertext, 'secret key 123');
var decryptedData = JSON.parse(bytes.toString());

console.log(decryptedData); // [{id: 1}, {id: 2}]
```

### List of modules 

- ```cryptojs2/md5```
- ```cryptojs2/sha1```
- ```cryptojs2/sha256```
- ```cryptojs2/sha224```
- ```cryptojs2/sha512```
- ```cryptojs2/sha384```
- ```cryptojs2/sha3```
- ```cryptojs2/ripemd160```

---

- ```cryptojs2/hmac-md5```
- ```cryptojs2/hmac-sha1```
- ```cryptojs2/hmac-sha256```
- ```cryptojs2/hmac-sha224```
- ```cryptojs2/hmac-sha512```
- ```cryptojs2/hmac-sha384```
- ```cryptojs2/hmac-sha3```
- ```cryptojs2/hmac-ripemd160```

---

- ```cryptojs2/pbkdf2```

---

- ```cryptojs2/aes```
- ```cryptojs2/tripledes```
- ```cryptojs2/rc4```
- ```cryptojs2/rabbit```
- ```cryptojs2/rabbit-legacy```



## License
MIT license; see [LICENSE](./LICENSE).
